class Donor < ActiveRecord::Base
  attr_accessible :email, :name
  has_many :donations
end
